def word_length_count(file_name):
    main_dict = {}
    file = open(file_name,"r")
    for i,line in enumerate(file):
        sec_dict = {}
        words = line.split()
        for word in words:
            if len(word) not in sec_dict:
                sec_dict[len(word)] = 1
            else:
                sec_dict[len(word)] += 1
        main_dict[i+1] = sec_dict
    print(main_dict,"\n",sec_dict)

word_length_count("data1.txt")