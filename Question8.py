def get_valid_integer():
    while True:
        user_input = input("Enter an integer: ")
        try:
            integer_value = int(user_input)
            return integer_value
        except ValueError:
            print("Invalid input. Please enter a valid integer.")


integer_value = get_valid_integer()
print("Valid integer entered:", integer_value)
