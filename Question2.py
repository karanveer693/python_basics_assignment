import time


def duration(func):
    def inner(x, y):
        start = time.time()
        func(x, y)
        end = time.time()
        return func(x, y)

    return inner

@duration
def add(x, y):
    print(x + y)

add(3,4)

