def sum_divisible_by_3_or_5(numbers):
    return sum(x for x in numbers if x % 3 == 0 or x % 5 == 0)

# test with a sample list
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
result = sum_divisible_by_3_or_5(numbers)
print(result)