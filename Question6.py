import csv

def read_csv_file(file_path):
    rows = []
    with open(file_path, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            rows.append(row)
    return rows


data = read_csv_file('data.csv')
print(data)
